// server.js

// set up ==========

// create our app w/ express
var express = require('express'); 
var app = express();

// mongoose for mongodb
var mongoose = require('mongoose');

// log requests to the console (express4)
var morgan = require('morgan');

// pull information from HTML POST (express4)
var bodyParser = require('body-parser');

// simulate DELETE and PUT (express4)
var methodOverride = require('method-override');

// configuration ======
// connect to mongoDB
mongoose.connect('mongodb://localhost/myapp'); 

// set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public'));

//log every request to the console
app.use(morgan('dev'));

//parse applicaiton/x-www-form-urlencoded
app.use(bodyParser.urlencoded({'extended':'true'}));

//parse application/json
app.use(bodyParser.json());

//parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 

app.use(methodOverride());

// define model ============
var Todo = mongoose.model('Todo', { 
  text : String
});

// routes ======================

// api ----------------

// get all todos
app.get('/api/todos', function(req, res) { 
  //use mongoose to get all todos in the database
  Todo.find(function(err, todos) { 
    
    // if there is an error retrieving, send the error. Nothing after res.send(err) will execute
    if(err)
      res.send(err)

    res.json(todos); // return all todos in JSON format
  });
});

//create todo and send back all todos after creation
app.post('/api/todos', function(req, res) { 
  
  // create a todo, information comes from AJAX request from Angular
  Todo.create({
    text : req.body.text,
    done : false
  }, function(err, todo) { 
    if(err) 
      res.send(err);

    // get and return all the todos after you create another
    Todo.find(function(err, todos) { 
      if(err)
        res.send(err)
      res.json(todos);
    });
  });

});


// delete a todo
app.delete('/api/todos/:todo_id', function(req, res) { 
  Todo.remove({
    _id : req.params.todo_id
  }, function(err, todo) { 
    if(err)
      res.send(err);

    // get and return all the todos after you create another
    Todo.find(function(err, todos) { 
      if(err) 
        res.send(err);
      res.json(todos);
    });
  });

});




// listend (start app with node server.js) ===================
app.listen(8080);
console.log("App is listening on port 8080");


